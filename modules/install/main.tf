module "namespace" {
  source   = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-namespace.git//modules/namespace"
  name     = var.namespace
  metadata = var.namespace_metadata
}

variable "address_pool_name" {
  default = "generic-cluster-pool"
}

locals {
  EXTRA_VALUES = {
    configInline = {
      address-pools = [{
        name      = var.address_pool_name
        protocol  = "layer2"
        addresses = [var.layer2_ip_range]
      }]
    }
  }
}

# https://github.com/bitnami/charts/tree/master/bitnami/metallb/#installing-the-chart
resource "helm_release" "release" {
  name       = "metallb"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "metallb"
  version    = var.chart_version
  namespace  = module.namespace.this.metadata[0].name
  values = [
    #"${file("${path.module}/files/values.yaml")}"
    yamlencode(local.EXTRA_VALUES)
  ]

  set {
    name  = "controller.image.repository"
    value = "metallb/controller"
  }
  set {
    name  = "controller.image.tag"
    value = var.image_tag
  }

  set {
    name  = "speaker.image.repository"
    value = "metallb/speaker"
  }
  set {
    name  = "speaker.image.tag"
    value = var.image_tag
  }
}
