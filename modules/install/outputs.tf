output "release" {
  value = helm_release.release
}

output "address_pool_name" {
  value = var.address_pool_name
}
output "namespace" {
  value = module.namespace.this
}

output "annotations" {
  value = {
    "metallb.universe.tf/allow-shared-ip" = "true"
    "metallb.universe.tf/address-pool"    = var.address_pool_name
  }
}
