variable "namespace" {
  default = "metallb"
}

variable "chart_version" {
  default = "0.1.24"
}

variable "extra_values" {
  default = {}
}

variable "image_tag" {
  default = "v0.9.3"
}

variable "layer2_ip_range" {

}

variable "namespace_metadata" {
  type = object({
    supporters = string
    solution   = string
  })
  default = {
    supporters = "cluster-admins"
    solution   = "technical"
  }
}
