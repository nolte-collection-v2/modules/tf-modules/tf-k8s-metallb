
module "install" {
  source          = "../../modules/install"
  layer2_ip_range = "192.168.1.240-192.168.1.250"
}

output "helm_release" {
  value = module.install.release
}
